//===---------- RTLs/mpi/src/rtl.h - MPI RTL Definition - C++ -----------*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Declarations for the MPI RTL plugin.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_OMPCLUSTER_MPI_MANAGER_H_
#define _OMPTARGET_OMPCLUSTER_MPI_MANAGER_H_

#include <cassert>
#include <ffi.h>
#define MPICH_SKIP_MPICXX
#include <list>
#include <map>
#include <mpi.h>
#include <mutex>
#include <stdint.h>
#include <unordered_map>
#include <vector>

#include "event_system.h"

#include "omptarget.h"

#include "mpi_common.h"
#include "BumpMemoryManager.h"

/// Forward declarations.
namespace ft {
class FaultTolerance;
class FailureInjector;
} // namespace ft

/// Array of Dynamic libraries loaded for this target.
struct DynLibTy {
  char file_name[50];
  void *handle;
};

/// Keep entries table per device.
struct FuncOrGblEntryTy {
  __tgt_target_table table;
};

struct DataSubmitArgs {
  int32_t device_id;
  void *tgt_ptr;
  void *hst_ptr;
  int64_t size;
};

/// Event queue type.
using EventQueue = std::list<event_system::EventPtr>;

/// A class responsible for interacting with device native runtime library to
/// allocate and free memory.
class MpiDeviceAllocatorTy : public DeviceAllocatorTy {
  const int device_id;
  event_system::EventSystem *event_system;

public:
  MpiDeviceAllocatorTy(int device_id, event_system::EventSystem *event_system)
      : device_id(device_id), event_system(event_system) {}

  void *allocate(size_t Size, void *, TargetAllocTy Kind) override;

  int free(void *TgtPtr) override;
};

/// Class containing all the device information.
class RTLDeviceInfoTy {
  std::vector<std::list<FuncOrGblEntryTy>> func_gbl_entries;

  // A vector of device allocators
  std::vector<MpiDeviceAllocatorTy> device_allocators;

  // A vector of memory managers. Since the memory manager is non-copyable and
  // non-removable, we wrap them into std::unique_ptr.
  std::vector<std::unique_ptr<BumpMemoryManagerTy>> memory_managers;

  // Whether use memory manager
  bool use_memory_manager = true;

  // DynBcast pending event list
  std::map<void *, event_system::EventPtr> dynbcast_map;

public:
  std::list<DynLibTy> dyn_libs;

  int number_workers;
  MPI_Comm target_comm;
  ft::FaultTolerance *ft_handler;
  ft::FailureInjector *fi_handler;
  event_system::EventSystem *event_system;
  std::list<void *> hst_bcast_ptrs;

  int world_rank;
  int use_bcast_event;

  // Record entry point associated with device.
  void createOffloadTable(int32_t device_id, __tgt_offload_entry *begin,
                          __tgt_offload_entry *end);

  // Return true if the entry is associated with device.
  bool findOffloadEntry(int32_t device_id, void *addr);

  // Return the pointer to the target entries table.
  __tgt_target_table *getOffloadEntriesTable(int32_t device_id);

  // Return the pointer to the target entries table.
  __tgt_target_table *getOffloadEntriesTableOnDevice();

  RTLDeviceInfoTy();

  ~RTLDeviceInfoTy();

  // Check whether a given DeviceId is valid
  bool isValidDeviceId(const int device_id) const;

  int getNumOfDevices() const;

  // Check whether is given binary is valid for the device
  int32_t isValidBinary(__tgt_device_image *image);

  __tgt_target_table *loadBinary(const int device_id,
                                 const __tgt_device_image *image);

  __tgt_target_table *loadBinaryOnDevice(const __tgt_device_image *image);

  // Register the shared library to the current device
  void registerLib(__tgt_bin_desc *desc);

  // Acquire the async context from the async info object. If no context is
  // present, a new one is created.
  EventQueue *getEventQueue(__tgt_async_info *async_info);

  // Push a new event to the respective device queue, updating the async info
  // context.
  void pushNewEvent(const event_system::EventPtr &event, int32_t device_id,
                    __tgt_async_info *async_info);

  // Adds a TgtPtr to a existing DynBcast event
  bool addPtrDynBcast(int device_rank, int32_t device_id, void *tgt_ptr,
                      void *hst_ptr, int64_t size);

  // Data management
  void *dataAlloc(int32_t device_id, int64_t size, void *hst_ptr);
  int32_t dataDelete(int32_t device_id, void *tgt_ptr);

  int32_t dataRecovery(int32_t device_id, void *tgt_ptr, int64_t size,
                       int32_t buffer_id, int32_t cp_rank, int32_t cp_ver);
  int32_t dataRecoveryAsync(int32_t device_id, void *tgt_ptr, int64_t size,
                            int32_t buffer_id, int32_t cp_rank, int32_t cp_ver,
                            __tgt_async_info *async_info);

  int32_t dataSubmit(int32_t device_id, void *tgt_ptr, void *hst_ptr,
                     int64_t size);
  int32_t dataSubmitAsync(int32_t device_id, void *tgt_ptr, void *hst_ptr,
                          int64_t size, __tgt_async_info *async_info);

  int32_t dataRetrieve(int32_t device_id, void *hst_ptr, void *tgt_ptr,
                       int64_t size);
  int32_t dataRetrieveAsync(int32_t device_id, void *hst_ptr, void *tgt_ptr,
                            int64_t size, __tgt_async_info *async_info);

  int32_t registerBcastPtr(int32_t arg_num, void **args_base, void **args,
                           int64_t *arg_sizes, int64_t *arg_types,
                           bool AddToList);

  int32_t dataExchange(int32_t src_id, void *src_ptr, int32_t dst_id,
                       void *dst_ptr, int64_t size);
  int32_t dataExchangeAsync(int32_t src_id, void *src_ptr, int32_t dst_id,
                            void *dst_ptr, int64_t size,
                            __tgt_async_info *async_info);

  // Target execution
  int32_t runTargetTeamRegion(int32_t device_id, void *tgt_entry_ptr,
                              void **tgt_args, ptrdiff_t *tgt_offsets,
                              int32_t arg_num, int32_t team_num,
                              int32_t thread_limit,
                              uint64_t loop_tripcount /*not used*/);
  int32_t runTargetTeamRegionAsync(int32_t device_id, void *tgt_entry_ptr,
                                   void **tgt_args, ptrdiff_t *tgt_offsets,
                                   int32_t arg_num, int32_t team_num,
                                   int32_t thread_limit,
                                   uint64_t loop_tripcount /*not used*/,
                                   __tgt_async_info *async_info);

  int32_t synchronize(int32_t device_id, __tgt_async_info *async_info,
                      int32_t task_id = -1);

  // Start device main for worker ranks
  int32_t startDeviceMain(__tgt_bin_desc *desc);

  // Synchronization event management
  // ===========================================================================
  // Allocates a shared pointer to an event.
  int32_t createEvent(int32_t ID, void **Event);

  // Destroys a shared pointer to an event.
  int32_t destroyEvent(int32_t ID, void *Event);

  // Binds Event to the last internal event present in the event queue.
  int32_t recordEvent(int32_t ID, void *Event, __tgt_async_info *AsyncInfo);

  // Adds the `Event` to the event queue so we can wait for it. `Event` might
  // come from another device event queue (even on another task context),
  // allowing two tasks to synchronize their inner events when needed (e.g.:
  // wait for a data to be submitted).
  int32_t waitEvent(int32_t ID, void *Event, __tgt_async_info *AsyncInfo);

  // Waits for the Event, blocking the caller thread.
  int32_t syncEvent(int32_t ID, void *Event);

  // Fault tolerance management
  // ===========================================================================
private:
  std::vector<int32_t> failed_tasks_ids;

public:
  // Register an function callback to listen to FT notifications
  int32_t registerFTCallback(void *callback);
  // Register the ptrs to be saved by the device
  int32_t registerCPPtrs(int32_t device_id, uintptr_t *ptrs_to_register,
                         int64_t *ptrs_size, int32_t num_ptrs);
  int32_t registerCPPtrsAsync(int32_t device_id, uintptr_t *ptrs_to_register,
                              int64_t *ptrs_size, int32_t num_ptrs,
                              __tgt_async_info *async_info);
  // Execute the checkpoint
  int32_t requestCheckpoint(int32_t device_id, int32_t *version);
  int32_t requestCheckpointAsync(int32_t device_id, int32_t *version,
                                 __tgt_async_info *async_info);
  int32_t registerFailedTasksId(int32_t number_of_tasks, int32_t *tasks_ids);
};

#endif // _OMPTARGET_OMPCLUSTER_MPI_MANAGER_H_
