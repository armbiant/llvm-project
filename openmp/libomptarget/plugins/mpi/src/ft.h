//===------ ft.h - Common MPI fault tolerance declarations ------*- C++ -*-===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// This file contains the declarations of fault tolerance class, functions,
// types and macros.
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_FTLIB_H_
#define _OMPTARGET_FTLIB_H_

#include <atomic>
#include <condition_variable>
#include <functional>
#include <map>
#define MPICH_SKIP_MPICXX
#include <mpi.h>
#include <thread>
#include <vector>

// These wrapping functions will be located outside of ft namespace since it
// will auto replace the MPI calls that are being wrapped
extern "C" {
// Reference for real wrapped functions (this refers to the MPI distribution
// implementations)
extern int __real_MPI_Wait(MPI_Request *request, MPI_Status *status);
extern int __real_MPI_Test(MPI_Request *request, int *flag, MPI_Status *status);
extern int __real_MPI_Barrier(MPI_Comm comm);
extern int __real_MPI_Comm_free(MPI_Comm *comm);
extern int __real_MPI_Send(const void *buf, int count, MPI_Datatype datatype,
                           int dest, int tag, MPI_Comm comm);
extern int __real_MPI_Recv(void *buf, int count, MPI_Datatype datatype,
                           int source, int tag, MPI_Comm comm,
                           MPI_Status *status);
// Custom wrapped functions (this refers to the functions implemented by this
// library)
int MPI_Iwait(MPI_Request *request, MPI_Status *status, int proc);
int MPI_Itest(MPI_Request *request, int *flag, MPI_Status *status, int proc);
int __wrap_MPI_Wait(MPI_Request *request, MPI_Status *status);
int __wrap_MPI_Test(MPI_Request *request, int *flag, MPI_Status *status);
int __wrap_MPI_Barrier(MPI_Comm comm);
int __wrap_MPI_Comm_free(MPI_Comm *comm);
int __wrap_MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest,
                    int tag, MPI_Comm comm);
int __wrap_MPI_Recv(void *buf, int count, MPI_Datatype datatype, int source,
                    int tag, MPI_Comm comm, MPI_Status *status);
}

class RTLDeviceInfoTy;

namespace ft {

// Heartbeat message tags definition
enum FTMPITags {
  TAG_HB_ALIVE = 0x70,
  TAG_HB_NEWOBS = 0x71,
  TAG_HB_BCAST = 0x72,
  TAG_CP_DONE = 0x73
};

enum HBBCTypes {
  // [0] HB_BC_FAILURE | [1] Dead process rank  | [2] Dead position | [3] Dummy
  HB_BC_FAILURE = 1,
  // [0] HB_BC_ALIVE   | [1] Alive process rank | [2] New  position | [3] Dummy
  HB_BC_ALIVE = 2,
  // [0] HB_BC_REPAIR  | [1] Check sum value    | [2] Current size  | [3] Dummy
  HB_BC_REPAIR = 3,
  // [0] CP_START      | [1] Process rank       | [2] CP counter    | [3] Dummy
  CP_START = 4,
  // [0] CP_COMPLETED  | [1] Process rank       | [2] CP counter    | [3] Cost
  CP_COMPLETED = 5,
  // [0] CP_CANCELED   | [1] Process rank       | [2] CP counter    | [3] Dummy
  CP_CANCELED
};

enum class ProcessState { DEAD, ALIVE };

enum class CommState { VALID, INVALID };

enum class FTNotificationID {
  FAILURE,
  FALSE_POSITIVE,
  CHECKPOINT,
  CHECKPOINT_DONE
};

enum class ExecState : int { RUNNING = 0, CHECKPOINTING = 1, RESTARTING = 2 };

// Thread Instructions
enum class FHInstruction { NONE, FAILURE, FALSE_POSITIVE, FINISH };
enum class IHInstruction { NONE, START_CP, CALC_INTERVAL, FINISH };
enum class CPInstruction { NONE, EXEC_CP, COMPLETE_CP, CANCEL_CP, FINISH };
enum class WTInstruction { NONE, TRANSFER, FINISH };

struct FTNotification {
  FTNotificationID notification_id;
  int value;
};

// (void *) to function pointer conversion
typedef void (*FtCallbackTy)(FTNotification); 

struct CPAllocatedMem {
  int id;           // Id given to a pointer
  void *pointer;    // Address of allocated memory
  size_t size;      // Size (in bytes) of allocated memory
  size_t base_size; // Control variable (true if needs cp)
};

// Default values for FT library
constexpr int DEFAULT_HB_TIMEOUT = 3000; // in ms
constexpr int DEFAULT_HB_TIME = 1000;    // in ms
constexpr int DEFAULT_HB_TIME_STEP = 50; // in ms

// Return values definition
constexpr int FT_VELOC_ERROR = -1;
constexpr int FT_VELOC_SUCCESS = 0;

// Fault tolerance definitions for wrappers
constexpr int FT_SUCCESS = 0;
constexpr int FT_ERROR = 1;
constexpr int FT_SUCCESS_NEW_COMM = 2;
constexpr int FT_MPI_COLLECTIVE = -1;
constexpr int FT_WAIT_LEGACY = -2;
constexpr int FT_TEST_LEGACY = -2;

// Fault tolerance class declaration
class FaultTolerance {
private:
  // MPI definitions
  int size, app_size;                // MPI size of communicator
  int rank;                          // MPI rank of node
  MPI_Comm main_comm, hb_comm;       // Communicator handlers
  MPI_Errhandler main_error_handler; // main_comm error handling object
  MPI_Errhandler hb_error_handler;   // hb_comm error handling object
  MPI_Request send_request;          // MPI required request in send functions
  MPI_Message msg_recv;              // Holds probed messages
  int msg_recv_flag;                 // Indicates received messages
  // MPI related functions
  template <typename T, typename S, typename R>
  int MPIw_IProbeRecv(T *buffer, int size, S type, int source, int tag,
                      MPI_Comm comm, R status);

  // MPI message buffers handlers
  int new_obs_msg, alive_msg, hb_message;
  int bc_message[4], bc_message_recv[4]; // Use separated to avoid concurrency

  // Fault tolerance general functions
  void setErrorHandling(); // Configure error handling procedures
  void commRepair();       // Repair communicator

  // Heartbeat variables
  int delta;     // Time for suspecting a failure
  int delta_to;  // Suspect time counter
  int eta;       // Period of hearbeat
  int eta_to;    // Heartbeat period counter
  int time_step; // Heartbeat thread loop time step
  int observer, emitter, n_pos;
  int failed_rank;
  bool hb_started, hb_need_repair;
  std::thread heartbeat;
  std::mutex hb_need_repair_mutex, hb_started_mutex, hb_finished_mutex;
  std::condition_variable hb_need_repair_cv, hb_started_cv, hb_finished_cv;
  std::atomic<bool> hb_done;
  std::atomic<ProcessState> *p_states;
  std::atomic<CommState> c_state;
  std::atomic<ExecState> exec_state;
  std::vector<int> neighbors;
  std::thread::id thread_id;
  std::vector<std::function<void(FTNotification)>> notify_callbacks;
  // Heartbeat functions
  void hbInit();                 // Initialize heartbeat
  void hbMain();                 // Main function of heartbeat
  void hbSendAlive();            // Send alive message for observer
  void hbResetObsTimeout();      // Reset suspect time-out
  void hbFindDeadNode();         // Set and broadcast emitter failure
  int hbFindEmitter();           // Find new emitter after failure
  void hbSetNewObs(int new_obs); // Set new observer after failure
  void hbBroadcast(int type, int first_value,
                   int second_value, int third_value); // Internal broadcast

  // Checkpointing variables
  std::string cp_cfg_file_path, cp_file_name;
  std::mutex cp_veloc_mutex;
  std::vector<CPAllocatedMem> cp_reg_pointers;
  std::vector<bool> cp_started_ranks;
  std::vector<bool> cp_completed_ranks;
  int cp_cost_sum, cp_last_sum, cp_local_cost, cp_interval;
  int cp_mtbf, cp_wspeed, cp_next_region_id, cp_count;
  // Checkpointing functions
  void cpInit(const char *loc);
  void cpEnd();
  void cpSetNextInterval();

  // Wait transfer thread control
  std::thread wait_transfer_thread;
  std::condition_variable wt_cv;
  std::mutex wt_mutex;
  WTInstruction wt_instruction;
  void transferHandler();

  // Interval handling thread control
  std::thread interval_thread;
  std::condition_variable ih_cv;
  std::mutex ih_mutex;
  IHInstruction ih_instruction;
  void intervalHandler();

  // Checkpoint handling thread control
  std::thread checkpoint_thread;
  std::condition_variable cp_cv;
  std::mutex cp_mutex;
  CPInstruction cp_instruction;
  void checkpointHandler();

  // Failure handling thread control
  std::thread failure_thread;
  std::condition_variable fh_cv;
  std::mutex fh_mutex;
  FHInstruction fh_instruction;
  void failureHandler();

public:
  // Constructors
  FaultTolerance(int hb_time, int susp_time, MPI_Comm global_comm);
  ~FaultTolerance();

  // User level checkpoint functions
  int cpSaveCheckpoint(int32_t version);
  void cpRegisterPointers(std::vector<uintptr_t> ptr,
                          std::vector<int64_t> sizes, size_t base_size,
                          int *id);
  void cpUnregisterPointers();
  void cpLoadStart();
  int cpLoadMem(int id, int rank, int ver, size_t count, size_t base_size,
                void *memregion);
  void cpLoadEnd();

  // User level general fault tolerance functions
  ProcessState getProcessState(int id); // Get state of desired process
  CommState getCommState();             // Get state of communicator
  MPI_Comm requestCommRepair();         // Host request to repair communicator
  MPI_Comm getMainComm();               // Return the main communicator
  void registerNotifyCallback(std::function<void(FTNotification)> callback);

  // Test purpose functions
  uint64_t getID(); // Return the thread id
  int getEmitter(); // Return current emitter

  // Wrapper helping functions
  int getRank();
  int getSize();
  void disableAsserts();
};

constexpr int INJECTOR_MAX_FAILURES = 1;
constexpr int DEFAULT_INJECTOR_INTERVAL = 20;

class FailureInjector {
private:
  int64_t interval;
  int64_t number_of_failures;
  int64_t max_failures;
  int32_t mpi_rank;

  // Failure injection thread control
  std::thread injector;
  std::condition_variable fi_cv;
  std::mutex fi_mutex;
  bool fi_condition;
  void injectFailure();

public:
  FailureInjector(int64_t time, RTLDeviceInfoTy *rtl);
  ~FailureInjector();
};

} // namespace ft

#endif // _OMPTARGET_FTLIB_H_
