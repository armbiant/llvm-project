// XFAIL: *
// RUN: %libomptarget-compilexx-run-check-ftmpi

#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <signal.h>

#include <chrono>
#include <functional>
#include <thread>

#include "ft.h"

using namespace ft;

void signalHandler(int signum) {
  std::this_thread::sleep_for(std::chrono::milliseconds(1500));
}

void notification_callback(FTNotification notify_handler) {
  switch (notify_handler.notification_id) {
  case FTNotificationID::FAILURE:
    printf("Failure detected on rank %d\n", notify_handler.value);
    break;
  case FTNotificationID::FALSE_POSITIVE:
    printf("False positive failure detected on rank %d\n",
           notify_handler.value);
    break;
  case FTNotificationID::CHECKPOINT:
    printf("Checkpoint notification\n");
    break;
  case FTNotificationID::CHECKPOINT_DONE:
    printf("Checkpoint done notification\n");
    break;
  }
}

class Dummy {
public:
  void notification_callback(FTNotification notify_handler) {
    switch (notify_handler.notification_id) {
    case FTNotificationID::FAILURE:
      printf("Failure detected on rank %d\n", notify_handler.value);
      break;
    case FTNotificationID::FALSE_POSITIVE:
      printf("False positive failure detected on rank %d\n",
             notify_handler.value);
      break;
    case FTNotificationID::CHECKPOINT:
      printf("Checkpoint notification\n");
      break;
    case FTNotificationID::CHECKPOINT_DONE:
      printf("Checkpoint done notification\n");
      break;
    }
  }
};

// This program tests the notification system to receive the information about
// fault tolerance events with multiple callbacks
int main(int argc, char *argv[]) {
  signal(SIGINT, signalHandler);
  int provided, rank;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm device_comm = MPI_COMM_WORLD;
  MPI_Comm_rank(device_comm, &rank);

  FaultTolerance *heartbeat = new FaultTolerance(100, 500, device_comm);
  Dummy dummy;
  if (rank == 0) {
    heartbeat->registerNotifyCallback(notification_callback);
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  // CHECK: Failure detected on rank 1
  if (rank == 1) {
    pthread_kill(heartbeat->getID(), SIGINT);
  }

  // CHECK: False positive failure detected on rank 1
  std::this_thread::sleep_for(std::chrono::milliseconds(2500));

  delete heartbeat;
  MPI_Finalize();
  return EXIT_SUCCESS;
}
