// XFAIL: *
// RUN: %libomptarget-compilexx-run-check-ftmpi

#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <signal.h>

#include <chrono>
#include <thread>

#include "ft.h"

using namespace ft;

std::atomic<bool> freeHandler(false);

void signalHandler(int signum) {
  while (!freeHandler) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));
  }
}

// This program tests FT wrappers to avoid deadlocking when calling MPI
// functions with dead processes
int main(int argc, char *argv[]) {
  signal(SIGINT, signalHandler);
  int provided, rank;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm device_comm = MPI_COMM_WORLD;
  MPI_Comm_rank(device_comm, &rank);

  FaultTolerance *ft = new FaultTolerance(100, 500, device_comm);
  // Disable asserts for testing
  ft->disableAsserts();

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  if (rank == 1) {
    pthread_kill(ft->getID(), SIGINT);
  } else {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    int buffer[1024];
    int result;
    if (rank == 0) {
      // Wait for a msg from dead process
      result =
          MPI_Recv(buffer, 1024, MPI_INT, 1, 1, device_comm, MPI_STATUS_IGNORE);
      // CHECK: MPI_Recv returned with value FT_ERROR
      printf("MPI_Recv returned with value %s\n",
             (result == FT_SUCCESS) ? "FT_SUCCESS" : "FT_ERROR");

      // Test MPÌ_Iwait
      MPI_Request recv_req;
      MPI_Irecv(buffer, 1024, MPI_INT, 1, 1, device_comm, &recv_req);
      result = MPI_Iwait(&recv_req, MPI_STATUS_IGNORE, 1);
      // CHECK: MPI_Iwait returned with value FT_ERROR
      printf("MPI_Iwait returned with value %s\n",
             (result == FT_SUCCESS) ? "FT_SUCCESS" : "FT_ERROR");

      // Send message to rank 2
      result = MPI_Send(buffer, 1024, MPI_INT, 2, 2, device_comm);
      // CHECK: MPI_Send returned with value FT_SUCCESS
      printf("MPI_Send returned with value %s\n",
             (result == FT_SUCCESS) ? "FT_SUCCESS" : "FT_ERROR");

    } else if (rank == 2) {

      MPI_Request mprobe_req;
      MPI_Message msg;
      // Wait for message from rank 0
      result = MPI_Mprobe(0, 2, device_comm, &msg, MPI_STATUS_IGNORE);
      // CHECK: MPI_Mprobe returned with value FT_SUCCESS
      printf("MPI_Mprobe returned with value %s\n",
             (result == FT_SUCCESS) ? "FT_SUCCESS" : "FT_ERROR");
      if (result == FT_SUCCESS) {
        MPI_Mrecv(buffer, 1024, MPI_INT, &msg, MPI_STATUS_IGNORE);
      }
    }

    result = MPI_Comm_free(&device_comm);
    if (rank == 2) {
      // CHECK: MPI_Comm_free returned with value FT_ERROR
      printf("MPI_Comm_free returned with value %s\n",
             (result == FT_SUCCESS) ? "FT_SUCCESS" : "FT_ERROR");
    }

    result = MPI_Barrier(device_comm);
    if (result == FT_SUCCESS_NEW_COMM) {
      device_comm = ft->getMainComm();
      if (rank == 0) {
        // CHECK: MPI_Barrier returned with value FT_SUCCESS_NEW_COMM
        printf("MPI_Barrier returned with value %s\n",
               (result == FT_SUCCESS_NEW_COMM) ? "FT_SUCCESS_NEW_COMM"
                                               : "FT_ERROR");
        // CHECK: Comm is valid
        printf("Comm is %s\n",
               (ft->getCommState() == CommState::VALID) ? "valid" : "invalid");
      }
    }
  }
  if (rank == 1)
    freeHandler = true;
  delete ft;
  MPI_Finalize();
  return EXIT_SUCCESS;
}
