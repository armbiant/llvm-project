// XFAIL: *
// RUN: %libomptarget-compilexx-run-check-ftmpi

#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <signal.h>

#include <chrono>
#include <thread>

#include "ft.h"

using namespace ft;

void signalHandler(int signum) {
  std::this_thread::sleep_for(std::chrono::milliseconds(1500));
}

// This program tests for heartbeat reallocation when a failure occurs and
// reallocation when a false positve failure occurs
int main(int argc, char *argv[]) {
  signal(SIGINT, signalHandler);
  int provided, rank;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm device_comm = MPI_COMM_WORLD;
  MPI_Comm_rank(device_comm, &rank);

  FaultTolerance *heartbeat = new FaultTolerance(100, 500, device_comm);

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  if (rank == 1) {
    pthread_kill(heartbeat->getID(), SIGINT);
  }

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  // CHECK: Rank 0 is the rank 2 emitter
  if (rank == 2)
    printf("Rank %d is the rank %d emitter\n", heartbeat->getEmitter(), rank);

  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  // CHECK: Rank 1 is the rank 2 emitter
  if (rank == 2)
    printf("Rank %d is the rank %d emitter\n", heartbeat->getEmitter(), rank);

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  delete heartbeat;
  MPI_Finalize();
  return EXIT_SUCCESS;
}
