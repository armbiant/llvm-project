// XFAIL: *
// RUN: %libomptarget-compilexx-run-check-ftmpi

#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <signal.h>

#include <atomic>
#include <chrono>
#include <thread>

#include "ft.h"

using namespace ft;

std::atomic<bool> freeHandler(false);

void signalHandler(int signum) {
  while (!freeHandler) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));
  }
  freeHandler = false;
}

// This program tests the states of MPI comm when failure occurs and MPI comm
// after repair process
int main(int argc, char *argv[]) {
  signal(SIGINT, signalHandler);
  int provided, rank;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm device_comm = MPI_COMM_WORLD;
  MPI_Comm_rank(device_comm, &rank);

  FaultTolerance *heartbeat = new FaultTolerance(100, 500, device_comm);

  std::this_thread::sleep_for(std::chrono::milliseconds(500));
  if (rank == 1) {
    pthread_kill(heartbeat->getID(), SIGINT);
  } else {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    // CHECK: Comm is invalid
    if (rank == 0) {
      printf("Comm is %s\n", (heartbeat->getCommState() == CommState::VALID)
                                 ? "valid"
                                 : "invalid");
    }
    // Request repair in every process
    device_comm = heartbeat->requestCommRepair();

    // CHECK: Comm is valid
    if (rank == 0) {
      printf("Comm is %s\n", (heartbeat->getCommState() == CommState::VALID)
                                 ? "valid"
                                 : "invalid");
    }
    MPI_Barrier(device_comm);
  }

  if (rank == 1) {
    freeHandler = true;
    while (freeHandler);
  }
  delete heartbeat;
  MPI_Finalize();
  return EXIT_SUCCESS;
}