// XFAIL: *
// RUN: export OMPCLUSTER_CP_MTBF=2 && %libomptarget-compilexx-run-check-ftmpi-veloc

#include <cstdio>
#include <cstdlib>
#include <mpi.h>
#include <signal.h>

#include <atomic>
#include <chrono>
#include <functional>
#include <thread>

#include "ft.h"

using namespace ft;

std::atomic<bool> checkpoint(false);
std::atomic<bool> checkpoint_done(false);

void notification_callback(FTNotification notify_handler) {
  switch (notify_handler.notification_id) {
  case FTNotificationID::CHECKPOINT:
    checkpoint = true;
    break;
  case FTNotificationID::CHECKPOINT_DONE:
    checkpoint_done = true;
    break;
  case FTNotificationID::FALSE_POSITIVE:
  case FTNotificationID::FAILURE:
    break;
  }
}

// This program tests the library checkpointing interface
int main(int argc, char *argv[]) {
  int provided, rank;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  MPI_Comm device_comm = MPI_COMM_WORLD;
  MPI_Comm_rank(device_comm, &rank);

  FaultTolerance *heartbeat = new FaultTolerance(100, 500, device_comm);

  heartbeat->registerNotifyCallback(notification_callback);

  int buffer[1] = {rank}, id;
  std::vector<uintptr_t> ptrs = {(uintptr_t)buffer};
  std::vector<int64_t> sizes = {1};
  heartbeat->cpRegisterPointers(ptrs, sizes, sizeof(int), &id);
  if (id != 0)
    return 1;

  while (!checkpoint)
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

  int version = heartbeat->cpSaveCheckpoint(0);

  // Wait for checkpoint to complete
  while (!checkpoint_done)
    std::this_thread::sleep_for(std::chrono::milliseconds(10));

  // Loads the save checkpoints
  if (rank == 0) {
    int buffer_1[1], buffer_2[2];
    heartbeat->cpLoadStart();
    int r1 = heartbeat->cpLoadMem(0, 1, version, 1, sizeof(int), buffer_1);
    int r2 = heartbeat->cpLoadMem(0, 2, version, 1, sizeof(int), buffer_2);
    heartbeat->cpLoadEnd();

    // CHECK: Loaded 1 from rank 1 and 2 from rank 2
    printf("Loaded %d from rank 1 and %d from rank 2\n",
           (r1 != FT_VELOC_ERROR) ? *buffer_1 : 0,
           (r2 != FT_VELOC_ERROR) ? *buffer_2 : 0);
  }

  delete heartbeat;
  MPI_Finalize();
  return EXIT_SUCCESS;
}
