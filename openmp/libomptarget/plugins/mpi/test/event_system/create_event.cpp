// XFAIL: *
// RUN: %libomptarget-compilempicxx-mpirun-and-check

#include <cassert>
#include <cstdio>
#include <mpi.h>

#include "event_system.h"
#include "mpi_common.h"

using namespace event_system;

void exitEventSystem(EventSystem &event_system, int world_size) {
  for (int rank = 1; rank < world_size; ++rank) {
    auto event = event_system.createEvent<ExitEvent>(rank);
    event->wait();
  }
}

int main() {
  WMPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE);

  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int world_size = -1;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  assert(world_size > 1);

  {
    EventSystem event_system(MPI_COMM_WORLD, nullptr);

    if (rank == 0) {
      auto event =
          event_system.createEvent<DebugEvent>(1, "This is a debug message");

      // CHECK: New event pointer was not null
      printf("New event pointer %s null\n",
             event == nullptr ? "was" : "was not");

      // CHECK: Action type is Origin
      printf("Action type is %s\n", toString(event->event_location));

      // CHECK: Event type is Debug
      printf("Event type is %s\n", toString(event->event_type));

      // CHECK: Dest rank is 1
      printf("Dest rank is %d\n", event->dest_rank);

      // CHECK: Orig rank is 0
      printf("Orig rank is %d\n", event->orig_rank);

      // CHECK: Event tag is 1
      printf("Event tag is %d\n", event->mpi_tag);

      // CHECK: Event state before run is {{Created}}
      printf("Event state before run is %s\n",
             toString(event->getEventState()));

      event->wait();

      // CHECK: Event state after wait is Finished
      printf("Event state after wait is %s\n",
             toString(event->getEventState()));

      exitEventSystem(event_system, world_size);
    } else {
      event_system.runGateThread();
    }
  }

  MPI_Finalize();

  return 0;
}
