// XFAIL: *
// RUN: %libomptarget-compilempicxx-mpirun-and-check

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <mpi.h>

#include <vector>

#include "event_system.h"
#include "mpi_common.h"

using namespace event_system;

std::uintptr_t allocData(EventSystem &event_system, int size) {
  std::uintptr_t remote_address = 0;
  auto event = event_system.createEvent<AllocEvent>(1, size, &remote_address);

  // CHECK: New event pointer was not null
  printf("New event pointer %s null\n", event == nullptr ? "was" : "was not");

  // CHECK: Action type is Origin
  printf("Action type is %s\n", toString(event->event_location));

  // CHECK: Event type is Alloc
  printf("Event type is %s\n", toString(event->event_type));

  // CHECK: Dest rank is 1
  printf("Dest rank is %d\n", event->dest_rank);

  // CHECK: Orig rank is 0
  printf("Orig rank is %d\n", event->orig_rank);

  // CHECK: Event tag is 1
  printf("Event tag is %d\n", event->mpi_tag);

  // CHECK: Event state before run is {{Created}}
  printf("Event state before run is %s\n", toString(event->getEventState()));

  event->wait();

  // CHECK: Event state after wait is Finished
  printf("Event state after wait is %s\n", toString(event->getEventState()));

  return remote_address;
}

void sendData(EventSystem &event_system, std::vector<char> &source,
              std::uintptr_t remote_address, int size) {
  auto event = event_system.createEvent<SubmitEvent>(
      1, &source[0], reinterpret_cast<void *>(remote_address), size);

  // CHECK: New event pointer was not null
  printf("New event pointer %s null\n", event == nullptr ? "was" : "was not");

  // CHECK: Action type is Origin
  printf("Action type is %s\n", toString(event->event_location));

  // CHECK: Event type is Submit
  printf("Event type is %s\n", toString(event->event_type));

  // CHECK: Dest rank is 1
  printf("Dest rank is %d\n", event->dest_rank);

  // CHECK: Orig rank is 0
  printf("Orig rank is %d\n", event->orig_rank);

  // CHECK: Event tag is 2
  printf("Event tag is %d\n", event->mpi_tag);

  // CHECK: Event state before run is {{Created}}
  printf("Event state before run is %s\n", toString(event->getEventState()));

  event->wait();

  // CHECK: Event state after wait is Finished
  printf("Event state after wait is %s\n", toString(event->getEventState()));
}

std::vector<char> recvData(EventSystem &event_system,
                           std::uintptr_t remote_address, int size) {
  std::vector<char> remote_data(size, 0);
  auto event = event_system.createEvent<RetrieveEvent>(
      1, &remote_data[0], reinterpret_cast<void *>(remote_address), size);

  // CHECK: New event pointer was not null
  printf("New event pointer %s null\n", event == nullptr ? "was" : "was not");

  // CHECK: Action type is Origin
  printf("Action type is %s\n", toString(event->event_location));

  // CHECK: Event type is Retrieve
  printf("Event type is %s\n", toString(event->event_type));

  // CHECK: Dest rank is 1
  printf("Dest rank is %d\n", event->dest_rank);

  // CHECK: Orig rank is 0
  printf("Orig rank is %d\n", event->orig_rank);

  // CHECK: Event tag is 3
  printf("Event tag is %d\n", event->mpi_tag);

  // CHECK: Event state before run is {{Created}}
  printf("Event state before run is %s\n", toString(event->getEventState()));

  event->wait();

  // CHECK: Event state after wait is Finished
  printf("Event state after wait is %s\n", toString(event->getEventState()));

  return remote_data;
}

void deleteData(EventSystem &event_system, std::uintptr_t remote_address) {
  auto event = event_system.createEvent<DeleteEvent>(1, remote_address);

  // CHECK: New event pointer was not null
  printf("New event pointer %s null\n", event == nullptr ? "was" : "was not");

  // CHECK: Action type is Origin
  printf("Action type is %s\n", toString(event->event_location));

  // CHECK: Event type is Delete
  printf("Event type is %s\n", toString(event->event_type));

  // CHECK: Dest rank is 1
  printf("Dest rank is %d\n", event->dest_rank);

  // CHECK: Orig rank is 0
  printf("Orig rank is %d\n", event->orig_rank);

  // CHECK: Event tag is 4
  printf("Event tag is %d\n", event->mpi_tag);

  // CHECK: Event state before run is {{Created}}
  printf("Event state before run is %s\n", toString(event->getEventState()));

  event->wait();

  // CHECK: Event state after wait is Finished
  printf("Event state after wait is %s\n", toString(event->getEventState()));
}

void exitEventSystem(EventSystem &event_system, int world_size) {
  for (int rank = 1; rank < world_size; ++rank) {
    auto event = event_system.createEvent<ExitEvent>(rank);
    event->wait();
  }
}

int main() {
  WMPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE);

  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int world_size = -1;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  assert(world_size > 1);

  {
    EventSystem event_system(MPI_COMM_WORLD, nullptr);

    if (rank == 0) {
      constexpr int64_t local_size = 400e6;
      std::vector<char> local_data(local_size, 0);

      uint64_t local_sum = 0;
      for (int64_t i = 0; i < local_size; ++i) {
        local_data[i] = i % 255;
        local_sum += local_data[i];
      }

      std::uintptr_t remote_address = allocData(event_system, local_size);
      sendData(event_system, local_data, remote_address, local_size);
      auto remote_data = recvData(event_system, remote_address, local_size);
      deleteData(event_system, remote_address);

      uint64_t remote_sum = 0;
      for (int64_t i = 0; i < local_size; ++i) {
        remote_sum += remote_data[i];
      }

      // CHECK: Local and remote sums are equal
      printf("Local and remote sums %s equal\n",
             local_sum == remote_sum ? "are" : "are not");

      exitEventSystem(event_system, world_size);
    } else {
      event_system.runGateThread();
    }
  }

  MPI_Finalize();

  return 0;
}
