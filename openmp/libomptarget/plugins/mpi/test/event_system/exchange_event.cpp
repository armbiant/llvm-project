// XFAIL: *
// RUN: %libomptarget-compilempicxx-mpirun-and-check

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <mpi.h>

#include <vector>

#include "event_system.h"
#include "mpi_common.h"

using namespace event_system;

std::uintptr_t allocData(EventSystem &event_system, int dst_rank, int size) {
  std::uintptr_t remote_address = 0;

  auto event =
      event_system.createEvent<AllocEvent>(dst_rank, size, &remote_address);
  event->wait();

  return remote_address;
}

void sendData(EventSystem &event_system, int dst_rank,
              std::vector<char> &source, std::uintptr_t remote_address,
              int size) {
  auto event = event_system.createEvent<SubmitEvent>(
      dst_rank, &source[0], reinterpret_cast<void *>(remote_address), size);
  event->wait();
}

std::vector<char> recvData(EventSystem &event_system, int dst_rank,
                           std::uintptr_t remote_address, int size) {
  std::vector<char> remote_data(size, 0);
  auto event = event_system.createEvent<RetrieveEvent>(
      dst_rank, &remote_data[0], reinterpret_cast<void *>(remote_address),
      size);
  event->wait();

  return remote_data;
}

void exchangeData(EventSystem &event_system, int src_rank, int dst_rank,
                  const std::uintptr_t src_ptr, std::uintptr_t dst_ptr,
                  int size) {
  auto event = event_system.createEvent<ExchangeEvent>(
      src_rank, dst_rank, reinterpret_cast<const void *>(src_ptr),
      reinterpret_cast<void *>(dst_ptr), size);

  // CHECK: New event pointer was not null
  printf("New event pointer %s null\n", event == nullptr ? "was" : "was not");

  // CHECK: Action type is Origin
  printf("Action type is %s\n", toString(event->event_location));

  // CHECK: Event type is Exchange
  printf("Event type is %s\n", toString(event->event_type));

  // CHECK: Orig rank is 0
  printf("Orig rank is %d\n", event->orig_rank);

  // CHECK: Event state before run is {{Created}}
  printf("Event state before run is %s\n", toString(event->getEventState()));

  event->wait();

  // CHECK: Event state after wait is Finished
  printf("Event state after wait is %s\n", toString(event->getEventState()));
}

void deleteData(EventSystem &event_system, int dst_rank,
                std::uintptr_t remote_address) {
  auto event = event_system.createEvent<DeleteEvent>(dst_rank, remote_address);
  event->wait();
}

void exitEventSystem(EventSystem &event_system, int world_size) {
  for (int rank = 1; rank < world_size; ++rank) {
    auto event = event_system.createEvent<ExitEvent>(rank);
    event->wait();
  }
}

int main() {
  WMPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE);

  int rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int world_size = -1;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  assert(world_size > 1);

  {
    EventSystem event_system(MPI_COMM_WORLD, nullptr);

    if (rank == 0) {
      constexpr char local_size = 10;
      std::vector<char> local_data(local_size, 0);

      for (char i = 0; i < local_size; ++i) {
        local_data[i] = i;
      }

      std::uintptr_t rank_1_addr = allocData(event_system, 1, local_size);
      std::uintptr_t rank_2_addr = allocData(event_system, 2, local_size);

      sendData(event_system, 1, local_data, rank_1_addr, local_size);

      exchangeData(event_system, 1, 2, rank_1_addr, rank_2_addr, local_size);

      auto rank_2_data = recvData(event_system, 2, rank_2_addr, local_size);

      deleteData(event_system, 1, rank_1_addr);
      deleteData(event_system, 2, rank_2_addr);

      // CHECK: Local and remote sizes are equal
      printf("Local and remote sizes %s equal\n",
             local_data.size() == rank_2_data.size() ? "are" : "are not");

      for (char i = 0; i < local_size; ++i) {
        // CHECK: Next local and remote values are equal
        printf("Next local and remote values %s equal\n",
               local_data[i] == rank_2_data[i] ? "are" : "are not");
      }

      exitEventSystem(event_system, world_size);
    } else {
      event_system.runGateThread();
    }
  }

  MPI_Finalize();

  return 0;
}
