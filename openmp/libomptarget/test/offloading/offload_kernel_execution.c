// RUN: %libomptarget-compilexx-run-and-check-generic

#include <stdio.h>
#include <omp.h>

int main(void) {

#pragma omp target
  {
    // CHECK: Execution of the target region
    printf("Execution of the target region\n");
  }

  return 0;
}
