//===---------------------- ft.h - Fault Tolerance ------------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Definitions for Fault Tolerance integration between MPI Plugin and Omptarget
//
//===----------------------------------------------------------------------===//

#ifndef _OMPTARGET_FTSUPPORT_H
#define _OMPTARGET_FTSUPPORT_H

#include "algorithms/basescheduler.h"
#include "device.h"

#include <atomic>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <unordered_map>
#include <vector>

/// \p RESTART_DEVICE is used to define a restarting task, if any function of
/// interface is called with this device, means that a call from a restart
/// procedure
constexpr int RESTART_DEVICE = -(0x80);

// This must match the implementation of FT MPI plugin
enum class FTNotificationID {
  FAILURE,
  FALSE_POSITIVE,
  CHECKPOINT,
  CHECKPOINT_DONE
};
// Used to control checkpointing status
enum class CPState : int { NOT_RUNNING = 0, RUNNING };
enum class CPDevState : int { NONE = 0, IN_PROGRESS, DONE };
// Defines the state of an OmpCluster device
enum class DeviceState : int { DEAD = 0, ALIVE };
// Used to control Restart status
enum class RestartState : int { IN_PROGRESS = 0, COMPLETED };
// Used to control the execution in the interface
enum class ExecutionState : int {
  RUNNING = 0,
  CHECKPOINTING,
  FAILED,
  RESTARTING
};
// Represents what interface function was called for each task
enum class InterfaceTaskFunction {
  itf_tgt_target_data_begin_mapper = 0,
  itf_tgt_target_data_end_mapper,
  itf_tgt_target_data_update_mapper,
  itf_tgt_target_kernel
};

// Defines a value for an invalid task
constexpr int INVALID_TASK = INT32_MIN;

// Defines the output of an notification
struct FTNotification {
  FTNotificationID notification_id;
  int value;
};

/// Defines the necessary args to re-execute a task
struct TaskInterfaceArgs {
  // Re-execute necessary definitions
  int32_t task_id = INVALID_TASK;
  InterfaceTaskFunction interface_function;
  // Task args (from the interface)
  ident_t *loc;
  int64_t device_id;
  void *host_ptr;
  int32_t arg_num;
  void **args_base;
  void **args;
  int64_t *arg_sizes;
  int64_t *arg_types;
  map_var_info_t *arg_names;
  void **arg_mappers;
  int32_t team_num;
  int32_t thread_limit;
  __tgt_kernel_arguments kernel_args;

  bool operator==(const kmp_target_task_data_t &r) {
    // TODO: Fix this comparison. Is this comparison sufficient?
    if (host_ptr == r.outlined_fn_id)
      return true;
    else
      return false;
  }
};

/// These structs holds all the info needed to save/load a buffer
struct PtrSaveInfo {
  uintptr_t hst_ptr, tgt_ptr;
  int64_t ptr_size;
  int32_t cp_buffer_id = -1, cp_device_id = -1;
};
struct DevSaveInfo {
  int32_t first_id = -1, last_id = -1, cp_version = -1;
  std::vector<PtrSaveInfo *> ptrs;
  std::vector<uintptr_t> tgt_ptrs;
  std::vector<int64_t> ptr_sizes;
  int32_t num_ptrs;
};

/// This struct holds the info about one pointer. It is used when restarting a
/// task
struct PtrRecoveryInfo {
  int32_t arg_num = 1;
  void *args_base;
  void *args;
  int64_t arg_sizes;
  int64_t arg_types;
  bool recovered = false;
  int32_t related_task = -1;
};

// Callback for handling FT notifications
void FTNotificationCallback(FTNotification notify_handler);

struct CPContext {
  std::unordered_map<uintptr_t, PtrSaveInfo> ptr_map;
  std::vector<DevSaveInfo> dev_map;
  std::vector<CPDevState> dev_state;
  std::vector<int32_t> saved_tasks;
  std::unordered_map<int32_t, bool> executing_tasks;
  bool canceled = false;
};

class FTNotificationHandler {
  // === General notification procedures
private:
  std::mutex internal_mtx;
  DeviceTy *proxy_device;
  RTLInfoTy *RTL;
  int32_t first_device_idx;

public:
  FTNotificationHandler();
  ~FTNotificationHandler() = default;

  void deactivate();
  bool isActive();

  // === Tasking and mapping related procedures
private:
  // Information about tasks in the last checkpoint
  ScheduleMap *schedule_map;
  kmp_target_task_map_t *task_graph;
  BaseScheduler *scheduler;
  // Map between <task id, device executing the task>
  std::unordered_map<int32_t, int> device_task_map;
  std::vector<int32_t> device_task_exec_count;
  // Map between <task id, interface function arguments>
  std::unordered_map<int32_t, TaskInterfaceArgs> task_interface_map;
  // Map between [current w_thread id] -> failed_task_id
  std::vector<int32_t> thread_task_map;

public:
  void RTLSetRefs();
  void setGraphRefs(ScheduleMap *sch_map, kmp_target_task_map_t *task_graph,
                    BaseScheduler *sch);
  bool isReferencesSet();
  void addDeviceTaskMapEntry(int32_t task_id, int device_id);
  void removeDeviceTaskMapEntry(int32_t task_id);
  void addFailedTask(int32_t task_id);

  // === Devices related procedures
private:
  std::mutex devices_mtx;
  std::vector<DeviceState> devices;
  int32_t devices_failed_count = 0;

public:
  void DevicesSetNumber(int32_t number_of_devices);
  void DevicesAddFailed(int device_id);
  void DevicesRemoveFailed(int device_id);
  void DevicesHandleFailed(int device_id);
  DeviceState DevicesCheck(int device_id);
  int32_t DevicesGetActiveNumber();
  int32_t DevicesTranslate(int device_id);

  // === Checkpointing related procedures
private:
  // cp_internal_mtx is used to control changes in CP context, like CPDevStates
  std::mutex cp_queue_mtx, cp_internal_mtx;
  std::queue<int32_t> cp_queue;

  std::atomic<CPState> cp_state;
  int32_t cp_next_version;
  CPContext cp_new, cp_last;

  void CPEvalBuffer(uintptr_t hst_ptr, int64_t ptr_size);
  void CPEvalTask(int32_t task_id);
  void CPSelectPtrs();
  void CPExec(int32_t device_id);
  void CPCancel();

public:
  bool CPCheck();
  void CPLaunch();
  void CPFinish(int version);

  // === Restarting related procedures
private:
  std::mutex restart_queue_mtx;
  std::queue<TaskInterfaceArgs> restart_queue;

  std::atomic<RestartState> restart_state;
  std::vector<int32_t> restart_failed_tasks;
  std::vector<int32_t> restart_task_list;
  std::atomic<int32_t> restart_task_counter;
  std::unordered_map<int32_t, bool> restarted_list;
  std::unordered_map<uintptr_t, PtrRecoveryInfo> restart_ptrs;

  void RestartCreateTaskList(const kmp_target_task_t *task,
                             int32_t call_task_id);
  void RestartTaskBuffer(std::vector<uintptr_t> hst_ptrs, int32_t device_id);
  void RestartEvalTask(int32_t task_id);
  void RestartExec(TaskInterfaceArgs &task, int device_id);
  void RestartFinishTask(const int task_id);

public:
  bool RestartCheck();
  void RestartLaunch(int device_id);

  // === Integration with OpenMP interface
private:
  std::atomic<ExecutionState> execution_state;
  std::atomic<int> running_tasks;

  // FT -> interface notify pair
  std::mutex ft_interface_mtx;
  std::condition_variable ft_interface_cv;

  // Interface -> FT notify pair
  std::mutex interface_ft_mtx;
  std::condition_variable interface_ft_cv;

  // History of executing tasks
  int max_task_id = 0;
  int32_t custom_id_counter = -2;

public:
  int32_t TaskRegisterArgs(InterfaceTaskFunction function, ident_t *loc,
                           int64_t device_id, void *host_ptr, int32_t arg_num,
                           void **args_base, void **args, int64_t *arg_sizes,
                           int64_t *arg_types, map_var_info_t *arg_names,
                           void **arg_mappers, int32_t team_num,
                           int32_t thread_limit,
                           __tgt_kernel_arguments kernel_args);
  void TaskFinish(int32_t task_id);
  int32_t TaskGetCurrentTargetID();
  bool TaskIsReady(int32_t task_id);
};

// Make the handler object visible to the entire interface
extern FTNotificationHandler ft_handler;

#endif /* _OMPTARGET_FTSUPPORT_H */
