//===----------- profiler.cpp - Chrome Tracing Profiler library -----------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
//
// Definitions for profiling an application into a Chrome Trace.
//
//===----------------------------------------------------------------------===//

#include "profiler.h"

#ifdef OMPTARGET_PROFILER

#include <algorithm>
#include <chrono>
#include <list>
#include <mutex>
#include <ratio>
#include <stack>
#include <string>
#include <vector>

#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <sys/time.h>

#include "llvm/Support/JSON.h"
#include "llvm/Support/Process.h"
#include "llvm/Support/Threading.h"

namespace _profiler {

// Config parameters.
// =============================================================================
constexpr char default_process_name[] = "Worker Process";
constexpr char default_thread_name[] = "Worker Thread";

// Profiler structures.
// =============================================================================
// Profile entry that measure the time between two points in the program.
namespace chrono = std::chrono;
using ProfilerDuration = chrono::duration<double, std::micro>;

struct ProfilerClock {
  using duration = chrono::nanoseconds;
  using rep = duration::rep;
  using period = duration::period;
  using time_point = chrono::time_point<ProfilerClock>;
  static const bool is_steady = false;

  static time_point now() noexcept {
    timespec time;
    // Use POSIX C-API for full compatibility with omptracing.
    clock_gettime(CLOCK_MONOTONIC_RAW, &time);
    return time_point(chrono::seconds(time.tv_sec) +
                      chrono::nanoseconds(time.tv_nsec));
  }
};

struct Entry {
  std::string name = "";    // Timeline label
  std::string details = ""; // Detailed description
  ProfilerClock::time_point start = ProfilerClock::time_point();
  ProfilerClock::time_point end = ProfilerClock::time_point();
};

// Profile entry that measure the time between two points in the program.
struct ThreadProfiler {
  std::string name = "";      // Timeline thread name
  uint64_t tid = 0;           // Thread ID
  int index = 0;              // Order in the thread list
  std::stack<Entry> stack;    // Entries currently active
  std::vector<Entry> entries; // Completed entries
};

struct ProcessProfiler {
  std::string name = "";                     // Timeline process name
  llvm::sys::Process::Pid pid = 0;           // Process ID
  int index = 0;                             // Order in the process list
  std::string filename = "";                 // Name for the dumped trace file
  bool enabled = false;                      // Enables the profiler.
  std::list<ThreadProfiler> threads_profile; // Process threads profilers
};

// Profiler global context.
// =============================================================================
// Global profiler context for the process.
static ProcessProfiler *process_profiler;
static std::mutex process_profiler_mtx;
// Local profiler for each thread.
static thread_local ThreadProfiler *thread_profiler;

// Helpers functions.
// =============================================================================
static double toProfileScale(ProfilerClock::time_point tp) {
  return chrono::duration_cast<ProfilerDuration>(tp.time_since_epoch()).count();
}

static double toProfileScale(ProfilerClock::duration d) {
  return chrono::duration_cast<ProfilerDuration>(d).count();
}

static std::string toLowerSnakeCase(const std::string &str) {
  std::string res = str;
  for (auto &c : res) {
    if (c == ' ') {
      c = '_';
    } else {
      c = std::tolower(c);
    }
  }
  return res;
}

// API functions.
// =============================================================================
void initProcessProfiler(std::string &&process_name, int index) {
  std::unique_lock<std::mutex> process_lk(process_profiler_mtx);

  // Strong check: immediately return if process profiler is already intialized.
  if (process_profiler != nullptr) {
    return;
  }

  if (process_name.empty()) {
    process_name = default_process_name;
  }

  // Create profile filename: filename_suffix + "_" + process_name + ".json"
  std::string filename = "";
  if (const char *env_str = std::getenv("OMPCLUSTER_PROFILE")) {
    filename =
        std::string(env_str) + "_" + toLowerSnakeCase(process_name) + ".json";
  } else if(const char *env_str = std::getenv("LIBOMPTARGET_PROFILE")) {
    // FIXME: Remove in the future
    fprintf(stderr, "[Profiler][Warning] LIBOMPTARGET_PROFILE is deprecated, "
                    "please use OMPCLUSTER_PROFILE instead!\n");
    filename =
        std::string(env_str) + "_" + toLowerSnakeCase(process_name) + ".json";
  }

  // Create a new process profiler.
  process_profiler = new ProcessProfiler{
      .name = std::move(process_name),
      .pid = llvm::sys::Process::getProcessId(),
      .index = index,
      .filename = filename,
      .enabled = !filename.empty(),
  };
}

void initThreadProfiler(std::string &&thread_name, int index) {
  // Weak check: init process profiler if it is needed.
  // Avoids double locking `process_profiler_mtx`.
  if (process_profiler == nullptr) {
    initProcessProfiler();
  }

  if (!process_profiler->enabled) {
    return;
  }

  std::unique_lock<std::mutex> process_lk(process_profiler_mtx);

  if (thread_profiler != nullptr) {
    return;
  }

  if (thread_name.empty()) {
    thread_name = default_thread_name;
  }

  // Create new thread profiler and set thread local reference.
  process_profiler->threads_profile.emplace_back(ThreadProfiler{
      .name = std::move(thread_name),
      .tid = llvm::get_threadid(),
      .index = index,
  });
  thread_profiler = &process_profiler->threads_profile.back();
}

void beginProfilePoint(const std::string &&name, const std::string &&details) {
  assert(name != "");

  // Weak check: init thread profiler if it is needed.
  // Avoids locking `process_profiler_mtx`.
  if (thread_profiler == nullptr) {
    initThreadProfiler();
  }

  if (!process_profiler->enabled) {
    return;
  }

  // Add new entry to local profiler stack.
  thread_profiler->stack.emplace(Entry{
      .name = std::move(name),
      .details = std::move(details),
      .start = ProfilerClock::now(),
  });
}

void endProfilePoint() {
  assert(process_profiler != nullptr);

  if (!process_profiler->enabled) {
    return;
  }

  assert(thread_profiler != nullptr);
  assert(!thread_profiler->stack.empty());

  // Finish top stack entry and move it to the entries list.
  Entry &entry = thread_profiler->stack.top();
  entry.end = ProfilerClock::now();
  thread_profiler->entries.emplace_back(entry);
  thread_profiler->stack.pop();
}

void dumpTracingFile() {
  assert(process_profiler != nullptr);

  if (!process_profiler->enabled) {
    return;
  }

  std::unique_lock<std::mutex> process_lk(process_profiler_mtx);

  assert(std::all_of(
      process_profiler->threads_profile.begin(),
      process_profiler->threads_profile.end(),
      [](const ThreadProfiler &tprof) { return tprof.stack.empty(); }));

  // Start file stream.
  std::error_code error_code;
  llvm::raw_fd_ostream file_stream(process_profiler->filename, error_code);
  if (error_code) {
    fprintf(
        stderr,
        "[Profiler][Warning] Could not open %s. Tracing was not generated!\n",
        process_profiler->filename.c_str());
    return;
  }

  // Start JSON file.
  llvm::json::OStream json(file_stream);
  json.object([&] {
    // ChromeTracing array of events (include traced events and metadata
    // ones).
    json.attributeArray("traceEvents", [&] {
      // Traced events.
      for (const auto &tprof : process_profiler->threads_profile) {
        for (const auto &entry : tprof.entries) {
          json.object([&] {
            json.attribute("ph", "X");
            json.attribute("name", entry.name);
            json.attribute("pid", process_profiler->pid);
            json.attribute("tid", static_cast<int64_t>(tprof.tid));
            json.attribute("ts", toProfileScale(entry.start));
            json.attribute("dur", toProfileScale(entry.end - entry.start));
            auto parsed_details = llvm::json::parse(entry.details);
            assert(parsed_details);
            json.attribute("args", parsed_details.get());
          });
        }
      }

      // Metadata.
      // Naming and ordering of processes.
      json.object([&] {
        json.attribute("ph", "M");
        json.attribute("name", "process_name");
        json.attribute("pid", process_profiler->pid);
        json.attributeObject(
            "args", [&] { json.attribute("name", process_profiler->name); });
      });
      json.object([&] {
        json.attribute("ph", "M");
        json.attribute("name", "process_sort_index");
        json.attribute("pid", process_profiler->pid);
        json.attributeObject("args", [&] {
          json.attribute("sort_index", process_profiler->index);
        });
      });

      // Naming and ordering of threads.
      for (const auto &tprof : process_profiler->threads_profile) {
        json.object([&] {
          json.attribute("ph", "M");
          json.attribute("name", "thread_name");
          json.attribute("pid", process_profiler->pid);
          json.attribute("tid", static_cast<int64_t>(tprof.tid));
          json.attributeObject("args",
                               [&] { json.attribute("name", tprof.name); });
        });
        json.object([&] {
          json.attribute("ph", "M");
          json.attribute("name", "thread_sort_index");
          json.attribute("pid", process_profiler->pid);
          json.attribute("tid", static_cast<int64_t>(tprof.tid));
          json.attributeObject(
              "args", [&] { json.attribute("sort_index", tprof.index); });
        });
      }
    });
  });
}

} // namespace _profiler

#endif // OMPTARGET_PROFILER